import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  movies = [];

  constructor(
    private router: Router,
    private moviesService: MoviesService
  ) {}

  ngOnInit(): void {
    this.moviesService.getMovies().then(moviesResponse => {
      this.movies = moviesResponse;
    })
  }

  goToDetailsPage(id) {
    this.router.navigate(['details', id]);
  }

}
