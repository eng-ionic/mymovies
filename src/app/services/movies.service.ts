import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieModel, MovieResponse } from '../models/movie-model';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  constructor(
    private httpClient: HttpClient
  ) { }

  getMovies(): Promise<Array<MovieModel>> {
    // Criando uma promise e retornando
    return new Promise((resolve, reject) => {
      // Nos Inscrevendo nos eventos de resposta do http cliente
      this.httpClient.get<MovieResponse>("https://api.themoviedb.org/3/movie/popular?api_key=51e4e9d52532d389174b5252cd99d33d")
        .subscribe(response => {
          // Dizendo a quem solicitou a informação que já terminou de 
          // carregar e retornando os dados da consulta
          resolve(response.results);
        }, error => {
          reject(error);
        });
    });
  }

  getMovie(id): Promise<MovieModel> {
    // Criando uma promise e retornando
    return new Promise((resolve, reject) => {
      // Nos Inscrevendo nos eventos de resposta do http cliente
      this.httpClient.get<MovieModel>(`https://api.themoviedb.org/3/movie/${id}?api_key=51e4e9d52532d389174b5252cd99d33d`)
        .subscribe(response => {
          // Dizendo a quem solicitou a informação que já terminou de 
          // carregar e retornando os dados da consulta
          resolve(response);
        }, error => {
          reject(error);
        });
    });
  }
}
