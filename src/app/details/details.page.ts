import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieModel } from '../models/movie-model';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  movie:MovieModel = {} as MovieModel;
  
  constructor(
    private activeRoute: ActivatedRoute,
    private moviesService: MoviesService
  ) {}

  ngOnInit() {
    this.moviesService
      .getMovie(this.activeRoute.snapshot.params.id)
      .then(response => {
        this.movie = response;
      });
  }

}
